﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public float CurrentHealth { get; set; }
    public float MaxHealth { get; set; }

    public Slider Healthbar;
    // Start is called before the first frame update
    void Start()
    {
        MaxHealth = 10f;
        CurrentHealth = MaxHealth;

        Healthbar.value = CalculateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
          //DealDamage(5);
    }

    void DealDamage(float damageValue)
    {
      CurrentHealth -= damageValue;
      Healthbar.value = CalculateHealth();
      if (CurrentHealth <= 0)
        Die();
    }

    float CalculateHealth()
    {
      return CurrentHealth / MaxHealth;
    }

    void Die()
    {
      CurrentHealth = 0;
      Debug.Log("You died.");
    }
}
