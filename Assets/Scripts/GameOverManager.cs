﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public Animator anim;
    private bool gameOver;

    void Start()
    {
        //anim = GetComponent<Animator>();
        gameOver = false;
    }

    void OnTriggerEnter(Collider other)
    {
      if (other.gameObject.CompareTag("Player"))
      {
        anim.SetTrigger("GameOver");
      }
    }

    // Update is called once per frame
    void Update()
    {
      //if (restartTimer >= 2f)
      //{
        //anim.SetTrigger("GameOver");
        //Transform.PlayerPosition = origin;
      //}
      //  if (PlayerPosition = Exit)
      //  {
      //    anim.SetTrigger("GameOver");
      //    restartTimer += Time.deltaTime;
        //  if (restartTimer >= restartDelay)
        //  {
          //  Application.LoadLevel(Application.LoadLevel);
        //  }
      //  }
    }
}
