﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playermovement : MonoBehaviour
{
    public float speed;
    //Rigidbody player;
    //public float turnSmoothing = 15f;
    //public float speedDampTime = 0.1f;
    Vector3 origin;
    float restartTimer;

    private Rigidbody rbody;
    //private Animator anim;
    //private HashIDs hash;

    void Start ()
    {
      rbody = GetComponent<Rigidbody>();
      origin = transform.position;
    }

    void FixedUpdate ()
    {
        //transform.Translate(speed*Input.GetAxis("Horizontal")*Time.deltaTime, 0f, speed*Input.GetAxis("Vertical")*Time.deltaTime);
        //rbody.AddForce(Input.GetAxis("Horizontal")*speed * Time.deltaTime, 0f, Input.GetAxis("Vertical")*speed * Time.deltaTime);
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        move = move.normalized; //player's speed in all direction will be the same
        move = move * speed * Time.deltaTime;
        rbody.MovePosition(rbody.position + move);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Detect"))
        {
            //print("game over");
            //if enemy detects the player, player is reset to his starting position
            transform.position = origin;
            restartTimer = 0f;
            SceneManager.LoadScene("UnityFinal");
        }
    }

    void Update ()
    {
      restartTimer += Time.deltaTime;
      if (restartTimer >= 60f)
      {
        //after 60 seconds if player doesn't make it to the exit, player is reset to his starting position
        transform.position = origin;
        restartTimer = 0f;
      }
    }
}
