﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    bool playerInRange;
    public GameObject bombPrefab;
    float bombDetonateTimer;
    public ParticleSystem ExplosionEffect;
    //public float fuseTime;
    // Start is called before the first frame update
    void Start()
    {
      //Invoke("Explode", fuseTime);
    }

    // Update is called once per frame
    void Update()
    {
      if ((playerInRange == true) && Input.GetKeyDown(KeyCode.Space))
      //print ("Plant bomb!");
      {
      Instantiate(bombPrefab, transform.position, bombPrefab.transform.rotation, transform);
      Invoke("DestroyDoor", 3);
      Invoke("Explode", 3);
      }
    }

    void Explode()
    {
      var exp = GetComponent<ParticleSystem>();
      exp.Play(); //play the explosion effect when explode happens
      Destroy(gameObject, exp.duration);
    }

    void DestroyDoor()
    {
      Destroy(transform.parent.gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
      if (other.CompareTag("Player"))
      playerInRange = true;
    }

    void OnTriggerExit(Collider other)
    {
      if (other.CompareTag("Player"))
      playerInRange = false;
    }
}
