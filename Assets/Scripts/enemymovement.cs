﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemymovement : MonoBehaviour
{
    public Transform[] Waypoints;
    int target=0;
    public float speed;
    private Rigidbody rbody;
    // Start is called before the first frame update
    void Start()
    {
        rbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.Distance(transform.position, Waypoints[target].position)<0.1f)
        //check if enemy is at target position
        {
            target += 1; //proceed to new target
            if (target >= Waypoints.Length) //reached end of array
                target = 0; //go back to starting position
        }
        else
        {
            //move towards target waypoint
            Vector3 move = Waypoints[target].position-transform.position;
            move = move.normalized;
            move = move * speed * Time.deltaTime;
            rbody.MovePosition(rbody.position + move);
        }
    }
}
